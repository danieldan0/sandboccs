﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;

namespace sandboccs.Utils
{
    public class TextureAtlas
    {
        readonly Dictionary<String, Texture2D> textureMap;

        public TextureAtlas()
        {
            textureMap = new Dictionary<string, Texture2D>();
        }

        public Texture2D GetTexture(String name) {
            return textureMap[name];
        }

        public bool AddTexture(String name, Texture2D texture)
        {
            if (textureMap.ContainsKey(name)) return false;
            textureMap.Add(name, texture);
            return true;
        }
    }
}
