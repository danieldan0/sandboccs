﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace sandboccs.Utils
{
    public class Draw
    {
        private static Color currentColour = Color.TransparentBlack;
        private static Texture2D pixel = null;

        public static void SetColour (Color colour) {
            currentColour = colour;
        }

        public static void DrawPixel(int x, int y)
        {
            if (pixel == null)
            {
                pixel = new Texture2D(Vars.renderer.graphics.GraphicsDevice, 1, 1);
                pixel.SetData(new[] { Color.White });
            }

            Vars.renderer.spriteBatch.Draw(pixel, new Vector2(x, y), currentColour);
        }

        public static void FillRectangle(int x, int y, int sizeX, int sizeY) {
            if (pixel == null)
            {
                pixel = new Texture2D(Vars.renderer.graphics.GraphicsDevice, 1, 1);
                pixel.SetData(new[] { Color.White });
            }

            Vars.renderer.spriteBatch.Draw(pixel, new Rectangle(x, y, sizeX, sizeY), currentColour);
        }

        public static void DrawRectangle(int x, int y, int sizeX, int sizeY)
        {
            if (pixel == null)
            {
                pixel = new Texture2D(Vars.renderer.graphics.GraphicsDevice, 1, 1);
                pixel.SetData(new[] { Color.White });
            }

            Vars.renderer.spriteBatch.Draw(pixel, new Rectangle(x, y, sizeX, 1), currentColour);
            Vars.renderer.spriteBatch.Draw(pixel, new Rectangle(x, y, 1, sizeY), currentColour);
            Vars.renderer.spriteBatch.Draw(pixel, new Rectangle(x + sizeX - 1, y, 1, sizeY), currentColour);
            Vars.renderer.spriteBatch.Draw(pixel, new Rectangle(x, y + sizeY - 1, sizeX, 1), currentColour);
        }

        public static void DrawLine(Vector2 point1, Vector2 point2, float thickness = 1f)
        {
            var distance = Vector2.Distance(point1, point2);
            var angle = (float)Math.Atan2(point2.Y - point1.Y, point2.X - point1.X);
            DrawLine(point1, distance, angle, thickness);
        }

        public static void DrawLine(Vector2 point, float length, float angle, float thickness = 1f)
        {
            var origin = new Vector2(0f, 0.5f);
            var scale = new Vector2(length, thickness);
            Vars.renderer.spriteBatch.Draw(pixel, point, null, currentColour, angle, origin, scale, SpriteEffects.None, 0);
        }

        public static void DrawQuad(GraphicsDevice graphicsDevice, Vector2 bottomLeft, Vector2 topRight)
        {
            var quad = new Quad();
            quad.Render(graphicsDevice, bottomLeft, topRight);
        }
    }
}
