﻿using System;
namespace sandboccs.Utils.Types
{
    public abstract class Collider
    {
        public abstract Boolean Collide();
    }
}
