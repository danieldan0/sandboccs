﻿using sandboccs.Core;
using sandboccs.Utils;
using sandboccs.World.Types.Entities;

namespace sandboccs
{
    public static class Vars
    {
        public static TextureAtlas atlas;

        public static string path;
        public static string gameDirectory;

        public static Microsoft.Xna.Framework.Game game;
        public static Renderer renderer;
        public static Networking networking;
        public static Logic logic;
        public static Core.World world;
        public static Control control;

        public static Player player;

        // In chunks; small for now 
        public static long worldSizeX = 16;
        public static long worldSizeY = 16;
        public static long worldSizeZ = 16;

        //In tiles; small for now
        public static byte chunkSizeX = 8;
        public static byte chunkSizeY = 8; //maybe make int for bigger chunks
        public static byte chunkSizeZ = 8;

        public static byte tileSizeX = 8;
        public static byte tileSizeZ = 8;

        public static void Initialize()
        {
            renderer = new Renderer();
            networking = new Networking();
            logic = new Logic();
            world = new Core.World();
            control = new Control();
            player = new Player();
        }
    }
}
