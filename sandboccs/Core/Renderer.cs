﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using sandboccs.World;
using sandboccs.World.Types;
using sandboccs.Utils;
using sandboccs.Visibility;

namespace sandboccs.Core
{
    public class Renderer
    {
        public GraphicsDeviceManager graphics;
        public SpriteBatch spriteBatch;

        RenderTarget2D tileMap;
        RenderTarget2D colourMap;
        RenderTarget2D lightMap;
        RenderTarget2D blurMap;

        Visual[] blocks;
        PointLight[] lights;

        public Renderer()
        {
            graphics = new GraphicsDeviceManager(Vars.game);
        }

        public void OnContentLoad() {
            spriteBatch = new SpriteBatch(graphics.GraphicsDevice);

            int windowWidth = graphics.GraphicsDevice.Viewport.Width;
            int windowHeight = graphics.GraphicsDevice.Viewport.Height;
            
            tileMap = new RenderTarget2D(graphics.GraphicsDevice, windowWidth, windowHeight, false, SurfaceFormat.Color, DepthFormat.None, 16, RenderTargetUsage.DiscardContents);
            colourMap = new RenderTarget2D(graphics.GraphicsDevice, windowWidth, windowHeight, false, SurfaceFormat.Color, DepthFormat.Depth16, 16, RenderTargetUsage.DiscardContents);
            lightMap = new RenderTarget2D(graphics.GraphicsDevice, windowWidth, windowHeight, false, SurfaceFormat.Color, DepthFormat.Depth16, 16, RenderTargetUsage.DiscardContents);
            blurMap = new RenderTarget2D(graphics.GraphicsDevice, windowWidth, windowHeight, false, SurfaceFormat.Color, DepthFormat.None, 16, RenderTargetUsage.DiscardContents);
        }

        public void Render() {
            spriteBatch.Begin();

            // Rectangle test
            Texture2D rect = new Texture2D(graphics.GraphicsDevice, 1, 1);

            rect.SetData(new[] { Color.White });

            Vector2 position = new Vector2(10f, 20f);
            Color color = Color.Chocolate;
            float rotation = 0.1f;
            Vector2 origin = Vector2.Zero;
            Vector2 scale = new Vector2(80f, 30f);
            SpriteEffects effects = SpriteEffects.None;
            float layerDepth = 0f;

            spriteBatch.Draw(rect, position, null, color, rotation, origin, scale, effects, layerDepth);

            // Another rectangle test
            Draw.SetColour(Color.Red);
            Draw.DrawRectangle(100, 100, 16, 16);

            // Line test
            Draw.SetColour(Color.Green);
            Draw.DrawLine(new Vector2(300, 300), new Vector2(400, 400), 10f);

            // Quad test
            BasicEffect effect = new BasicEffect(graphics.GraphicsDevice);
            effect.CurrentTechnique.Passes[0].Apply();
            Draw.DrawQuad(graphics.GraphicsDevice, new Vector2(-0.1f, -0.1f), new Vector2(0.1f, 0.1f));

            Vars.player.Draw();

            Vars.world.Draw(0, 0, 0);

            spriteBatch.End();
        }

        void DrawColorMap()
        {
            graphics.GraphicsDevice.SetRenderTarget(colourMap);
            graphics.GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, SamplerState.AnisotropicClamp, DepthStencilState.Default, RasterizerState.CullCounterClockwise, null);

            foreach (Visual v in blocks)
            {
                Vector2 origin = new Vector2(v.Texture.Width / 2.0f, v.Texture.Height / 2.0f);

                spriteBatch.Draw(v.Texture, v.Pose.Position, null, Color.White, v.Pose.Rotation, origin, v.Pose.Scale, SpriteEffects.None, 0);
            }

            spriteBatch.End();
        }

        void DrawLightMap(float ambientLightStrength)
        {
            graphics.GraphicsDevice.SetRenderTarget(lightMap);
            graphics.GraphicsDevice.Clear(Color.White * ambientLightStrength);

            // Draw normal object that emit light
            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, SamplerState.AnisotropicClamp, DepthStencilState.Default, RasterizerState.CullCounterClockwise);

            foreach (Visual v in blocks)
            {
                if (v.Glow != null)
                {
                    Vector2 origin = new Vector2(v.Glow.Width / 2.0f, v.Glow.Height / 2.0f);
                    spriteBatch.Draw(v.Glow, v.Pose.Position, null, Color.White, v.Pose.Rotation, origin, v.Pose.Scale, SpriteEffects.None, 0);
                }
            }

            spriteBatch.End();

            graphics.GraphicsDevice.BlendState = BlendState.Additive;
            // Samplers states are set by the shader itself            
            graphics.GraphicsDevice.DepthStencilState = DepthStencilState.None;
            graphics.GraphicsDevice.RasterizerState = RasterizerState.CullNone;

            foreach (PointLight l in lights)
            {
                l.Render(graphics.GraphicsDevice, blocks);
            }
        }
    }
}
