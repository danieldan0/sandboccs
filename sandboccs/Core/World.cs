﻿using System.Collections.Generic;
using sandboccs.World;

namespace sandboccs.Core
{
    public class World
    {
        public Chunk[,,] chunks;

        public World()
        {
            chunks = new Chunk[Vars.worldSizeX, Vars.worldSizeY, Vars.worldSizeZ];
        }

        // World coords
        public Chunk[] GetChunksInRange(long x, long y, long z, int range)
        {
            int chunkCoordY = (int)(x / Vars.chunkSizeX);
            int chunkCoordX = (int)(y / Vars.chunkSizeY);
            int chunkCoordZ = (int)(z / Vars.chunkSizeZ);
            if (chunks[chunkCoordX, chunkCoordY, chunkCoordZ] == null)
                GenerateChunk(chunkCoordX, chunkCoordY, chunkCoordZ);

            List<Chunk> value = new List<Chunk>();

            for (int lx = -range ; lx < range ; lx++) {
                for (int ly = -range; ly < range ; ly++) {
                    for (int lz = -range; lz < range ; lz++)
                    {
                        if (chunks[chunkCoordX + lx, chunkCoordY + ly, chunkCoordZ + lz] == null)
                            GenerateChunk(chunkCoordX + lx, chunkCoordY + ly, chunkCoordZ + lz);
                        value.Add(chunks[chunkCoordX + lx, chunkCoordY + ly, chunkCoordZ + lz]);
                    }
                }
            }

            return value.ToArray();
        }

        public void GenerateChunk(int x, int y, int z) {
            chunks[x, y, z] = new Chunk();
        }

        public void Draw(int x, int y, int z) {
            //Call Draw in chunks
        }
    }
}
