﻿using System;
using System.Collections.Generic;
using System.IO;
using YamlDotNet.Serialization;

namespace sandboccs.Core
{
    public static class Loader
    {
        public static void LoadGames() {
            Vars.path = Directory.GetCurrentDirectory();
            var gamesPath = Path.DirectorySeparatorChar+"games";

            if (!Directory.Exists(String.Join("", Vars.path, gamesPath)))
                Directory.CreateDirectory(String.Join("", Vars.path, gamesPath ));

            Vars.gameDirectory = String.Join("", Vars.path, gamesPath );

            foreach(String s in Directory.GetDirectories(Vars.gameDirectory)) {
                if(File.Exists(String.Join("", s, Path.DirectorySeparatorChar + "game.yaml"))) {
                    var deserializer = new DeserializerBuilder().Build();

                    var yaml = deserializer.Deserialize(new StreamReader(String.Join("", s, Path.DirectorySeparatorChar + "game.yaml")));
                    if (!(yaml is Dictionary<Object, Object>)) return;


                }
            }
        }
    }
}
