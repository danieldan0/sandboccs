﻿using Microsoft.Xna.Framework.Input;
using System;

namespace sandboccs.Core
{
    public class Control
    {
        public Control()
        {
        }

        public void Update() {
            KeyboardState state = Keyboard.GetState();
            if (state.IsKeyDown(Keys.Right))
            {
                Vars.player.Move(10, 0);
            }

            if (state.IsKeyDown(Keys.Left))
            {
                Vars.player.Move(-10, 0);
            }

            if (state.IsKeyDown(Keys.Up))
            {
                Vars.player.Move(0, -10);
            }

            if (state.IsKeyDown(Keys.Down))
            {
                Vars.player.Move(0, 10);
            }
        }
    }
}
