﻿using System;
using sandboccs.World.Types;

namespace sandboccs.World
{
    public class Tile
    {
        public byte block;
        public byte liquid;
        public byte liquidAmount;

        public Entity tileEntity;

        public Tile()
        {
        }

        public Block GetBlock() {
            return Block.GetByID(block);
        }

        public Liquid GetLiquid()
        {
            return null;
        }
    }
}
