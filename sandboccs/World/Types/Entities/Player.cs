﻿using System;
using Microsoft.Xna.Framework;

namespace sandboccs.World.Types.Entities
{
    public class Player : Entity
    {
        public Player()
        {
            x = 200;
            y = 200;
        }

        public override void Draw()
        {
            Utils.Draw.SetColour(Color.Black);
            Utils.Draw.FillRectangle((int) x, (int) y, 16, 16);
        }

        public void Move(long newX, long newY)
        {
            x += newX;
            y += newY;
        }
    }
}
