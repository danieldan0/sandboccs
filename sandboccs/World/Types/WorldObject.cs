﻿using System;
using sandboccs.Utils;
using sandboccs.Utils.Types;

namespace sandboccs.World.Types
{
    public class WorldObject
    {
        public long x, y;
        public byte sx, sy;

        public Collider collider;

        public WorldObject()
        {
        }
    }
}
