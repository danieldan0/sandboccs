﻿using System;
using System.Collections.Generic;

namespace sandboccs.World.Types
{
    public class Block
    {
        public static Dictionary<Byte, Block> blockDict;
        public static byte lastID;
        //Determines if block should update and have an entity
        public byte id;
        public Boolean update = false;

        public Block() {
            id = lastID++;
            blockDict.Add(id, this);
        }

        public virtual void Draw(Tile tile) {
        }

        public virtual void Update(Tile tile) {
        }

        public static Block GetByID(Byte block) {
            if (blockDict.ContainsKey(block)) return blockDict[block];
            return new Blocks.Empty();
        }
    }
}
