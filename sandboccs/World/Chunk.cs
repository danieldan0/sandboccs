﻿using System;
namespace sandboccs.World
{
    public class Chunk
    {
        //Only save chunk if changed
        public bool isDirty = false;
        public Tile[ , , ] tiles;

        public Chunk()
        {
            tiles = new Tile[Vars.chunkSizeX,Vars.chunkSizeY,Vars.chunkSizeZ];
        }

        public void Update() {
            foreach (Tile t in tiles) {
                if (t.GetBlock().update) t.GetBlock().Update(t);
            }
        }

        public void Draw(int y)
        {
            foreach (Tile t in tiles)
            {
                t.GetBlock().Draw(t);
            }
        }
    }
}
