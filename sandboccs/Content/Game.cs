﻿using System;
namespace sandboccs.Content
{
    public class Game
    {
        public String definitions;
        public String scripts;
        public String content;

        public Game(String definitions, String scripts, String content)
        {
            this.definitions = definitions;
            this.scripts = scripts;
            this.content = content;
        }
    }
}
