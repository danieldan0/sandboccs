﻿using System;
using Microsoft.Xna.Framework;

namespace sandboccs.Render
{
    public class Light
    {
        public float x, y;
        public Color color;

        public Light(float x, float y, Color color)
        {
            this.x = x;
            this.y = y;
            this.color = color;
        }
    }
}
